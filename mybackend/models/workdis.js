const mongoose = require('mongoose')
const workdisSchema = new mongoose.Schema({
    position: String,
    company: String,
    responsibility: String,
    characteristic: String,
    type_of_work: String,
    education: String,
    amount: Number,
    gender: String,
    salary: String,
    experience: String,
    place: String,
    security: String,
    contactname: String,
    address: String,
    phone: Number,
    homepage: String,

})

module.exports = mongoose.model('workdis', workdisSchema)