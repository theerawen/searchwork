const Workdis = require('../models/workdis')
const WorkdisController = {
    async addUser(req, res, next) {
        const payload = req.body
            // res.json(jobseekerController.addUser(payload))
        const user = new Workdis(payload)
        try {
            await user.save()
            res.json(user)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async updateUser(req, res, next) {
        const payload = req.body
            // res.json(jobseekerController.updateUser(payload))
        try {
            const user = await Workdis.updateOne({ _id: payload._id }, payload)
            res.json(user)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async deleteUser(req, res, next) {
        const { id } = req.params
            // res.json(jobseekerController.deleteUser(id))
        try {
            const user = await Workdis.deleteOne({ _id: id })
            res.json(user)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async getUsers(req, res, next) {
        // res.json(jobseekerController.getUsers())
        try {
            const users = await Workdis.find({})
            res.json(users)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async getUser(req, res, next) {
        const { id } = req.params
            // res.json(usersController.getUser(id))
            // User.findById(id).then(function (user) {
            //   res.json(user)
            // }).catch(function (err) {
            //   res.status(500).send(err)
            // })
        try {
            const user = await Workdis.findById(id)
            res.json(user)
        } catch (err) {
            res.status(500).send(err)
        }
    }
}
module.exports = WorkdisController